# coding: utf-8

""" Author: MORALES GABRIEL """

class Node:
    def __init__(self, value):
      """ initializes the node object class"""
      self.value = value
      self.left = None
      self.right = None

class DoubleChainedList:
    def __init__(self):
        """ initializes the doublechainedlist object class"""
        self.first_node = None

    def add_at_start(self, value_to_add):
        """Creates a node with the value given by the user and evaluates if there is a first node. If thats the case, it changes the pointers
        first_node.left to the new created node, the new_node.right pointing the first_node, and finally, sets the new node as self.first_node. 
        If self.first_node is None, it sets the new created node as self.first_node"""
        node_to_add = Node(value_to_add)
        if self.first_node is None:
            self.first_node = node_to_add
            return
        else:
            self.first_node.left = node_to_add
            node_to_add.right = self.first_node
            self.first_node = node_to_add

    def add_at_end(self,value_to_add):
        """Creates a node with the value given by the user and evaluates if there is a first node. If thats the case, it iterates over the list
        until it finds a node with the right atribbute as None (indicating the last node of the double linked list), then, it changes the right pointer
        of the current node to the new created node and the new_node.left pointing the current node.
        If self.first_node is None, it sets the new created node as self.first_node """
        node_to_add = Node(value_to_add)
        if self.first_node is None:
            self.first_node = node_to_add
            return
        temp = self.first_node
        while temp.right is not None:
            temp = temp.right
        temp.right = node_to_add
        node_to_add.left = temp
  
    def delete_begining(self):
        """ Function that evaluates if there the variable self.first_node is None (iondicating if is an empty list), if that the case, it shows a message
        indicating that the list is empty, if not, it evaluates if self.first_node is the only element on the list, if that the case, it sets self.fist_node as None.
        if not, it changes the atribbute self.first_node as the node pointing to the right, and finally, sets the attribute left of the right node (current self.first
        node) as None """
        if self.first_node is None:
            print("empty list")
            return
        if self.first_node.right is None:
            self.first_node = None
            return
        self.first_node = self.first_node.right
        self.first_node.left = None

    def delete_end(self):
        """ Function that evaluates if there the variable self.first_node is None (iondicating if is an empty list), if that the case, it shows a message
        indicating that the list is empty, if not, it evaluates if self.first_node is the only element on the list, if that the case, it sets self.fist_node as None.
        if not,it iterates over the list until it finds a node with the right atribbute as None (indicating the last node of the double linked list)
        and then sets the atribbute of the previous last node as NOne (Indicating that is the las element of the list)
        """
        if self.first_node is None:
            print("empty list")
            return
        if self.first_node.right is None:
            self.first_node = None
            return
        temp = self.first_node
        while temp.right is not None:
            temp = temp.right
        temp.left.right = None
            
    def delete_all_nodes(self):
        """FUnction that evaluates if the list is empty, and if is not the case, it calls the self.delete_begining() function to delete
        the first node, an then it calls it self of iniciate the process until the list is empty"""

        if self.first_node is None:
            print("empty list")
            return
        self.delete_begining()
        self.delete_all_nodes()

    
    def print_list(self) :
        """ prints chained list, iterating over all the elements of the list, in this case, it uses the comparation while temp is not None to be
        able to show all the elements of the list. """
        if self.first_node is None:
            print("The list is empty")
            return
        temp = self.first_node
        while temp is not None:
            print(temp.value)
            temp = temp.right


a = DoubleChainedList()

a.add_at_start(1.3)
a.add_at_start(2.3)
a.add_at_start(34.3)
a.add_at_start(3.3)

a.add_at_end(7.1)
a.add_at_end(7.1)
a.add_at_end(8.1)
a.print_list()
a.delete_end()

a.print_list()
a.delete_begining()
a.print_list()
a.delete_all_nodes()
a.print_list()